### Do Together

> Tooling

1. install node
2. install VS Code
3. get familiar with VS Code  
    * Preferences 
        * Settings (e.g. auto save)
        * Color Theme
    * Extensions (pomodoro)   
    * user snippets

> Ecosystem

1. *Terminal*
    * npm init
2. Add VSCode icons extension
3. index.js (console.log)
4. *Terminal*
    * node .\src\exercise-1\index.js
    * node .\src\exercise-1
    * package.json script
        * npm start
        * npm start:e1
5. NPM
    * indicators of good package
    * npm i --save chalk
    * inspect package.json and node_modules folder
    * update index.js

> Linting

1. npm i --save-dev eslint eslint-config-airbnb-base eslint-plugin-import
2. package.json lint script
   * npm run lint
3. Configure eslint
   * .\node_modules\.bin\eslint --init
3. Add ESlint extension

