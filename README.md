## Boardwalk JavaScript Workshop

> #### Goals

* JavaScript ecosystem
* JavaScript tooling
* Node
* Linting
* Development workflow

> #### Developer Setup

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:

``` bash
# Go to your projects directory
cd wherever-you-store-your-projects

# Clone this repository
git clone https://codell-concepts@bitbucket.org/codell-concepts/js-workshop-e1.git

# Go into the repository
cd js-workshop-e1

# install dependencies
npm install

```

> #### Running exercises

``` bash
# start exercise 1 (ecosystem)
npm run start:e1

```